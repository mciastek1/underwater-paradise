# Underwater Paradise
Simple app for underwater wildlife fans 🐠

Shipped with:
* [Handlebars](http://handlebarsjs.com/) templates
* ES6 support (provided by [Babel](https://babeljs.io/))
* [Sass/SCSS](http://sass-lang.com/) support
* Tests with [Mocha](https://mochajs.org/) and [Chai](http://chaijs.com/)

## Prerequisites
You will need the following things properly installed on your computer.
* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* alternatively you can use [Yarn](https://yarnpkg.com/en/)

## Setup
* `npm install` (or use `yarn install`)

## Running / Development
* `npm start` to serve templates
* Visit your app at [http://localhost:9000](http://localhost:9000).

### Building
* `npm run build` to build templates
* **Important!** - build task was not tested

## Testing
Unfortunately there are no tests 😢, maybe in the next version.

## Annotations
* Two-finger swipe, between cards, doesn't work on Android and iOS 10 device, due to not firing `touchend` event. It seems to be a browser bug, but it should be investigated later
* App was tested on iOS 9.1 Simulator (iPhone 6)
* App is somehow responsive, it's target device is iPhone 6/7
