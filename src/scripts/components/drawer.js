import debounce from 'lodash/debounce';

import Swiper from '../utils/swiper';
import prefixes from '../utils/prefix-detector';
import store from '../utils/store';
import resizeEventName from '../utils/resize-event-name';

const ELEMENT_CLASS = '.js-drawer';
const HEADER_CLASS = '.page-header';

const ANIMATION_CLASS_NAME = 'is-animate';
const HIDDEN_CLASS_NAME = 'is-hidden';

const defaults = {
  offset: 85,
};

class Drawer {
  constructor(element, options) {
    this.element = element;
    this.header = document.querySelector(HEADER_CLASS);

    this.swiper = new Swiper(element);
    this.store = store;

    this.settings = {
      ...defaults,
      ...options,
    };

    this.minOffset = 0;
    this.maxOffset = this.getMaxOffset();
    this.positionY = this.maxOffset;

    this.isOpen = false;

    this.init();
  }

  init() {
    this.attachEvents();
  }

  attachEvents() {
    this.swiper.addHandler('touchmove', this.handleTouchMove);
    this.swiper.addHandler('touchend', this.handleTouchEnd);
    window.addEventListener(resizeEventName, this.handleResize);
  }

  translate(posY) {
    this.element.style[prefixes.transform] = `translate3d(0, ${posY}px, 0)`;
  }

  toggle() {
    const newPositionY = (this.isOpen) ? this.maxOffset : this.minOffset;

    this.positionY = newPositionY;
    this.isOpen = !this.isOpen;

    this.store.drawerOpen = this.isOpen;

    this.header.classList.toggle(HIDDEN_CLASS_NAME, this.isOpen);

    this.animate();
    this.translate(newPositionY);
  }

  animate() {
    this.element.addEventListener(prefixes.transitionEnd, this.handleTransitionEnd);
    this.element.classList.add(ANIMATION_CLASS_NAME);
  }

  getMaxOffset() {
    const elementHeight = this.element.getBoundingClientRect().height;
    return elementHeight - this.settings.offset;
  }

  handleTouchMove = ({ y }, event) => {
    const newPositionY = y + this.positionY;

    if (newPositionY > this.minOffset && newPositionY <= this.maxOffset) {
      this.translate(newPositionY);
    }

    event.preventDefault();
  };

  handleTouchEnd = ({ time }, { y }) => {
    const duration = Date.now() - time;
    const elementHeight = this.element.getBoundingClientRect().height;

    const hasNewDirection = (y < 0 && !this.isOpen) || (y > 0 && this.isOpen);
    const shouldToggle = (duration < 300 && Math.abs(y) > 25) || (Math.abs(y) > elementHeight / 3);

    if (shouldToggle && hasNewDirection) {
      this.toggle();
    } else {
      this.animate();
      this.translate(this.positionY);
    }
  };

  handleTransitionEnd = () => {
    this.element.classList.remove(ANIMATION_CLASS_NAME);
    this.element.removeEventListener(prefixes.transitionEnd, this.handleTransitionEnd);
  };

  handleResize = debounce(() => {
    this.maxOffset = this.getMaxOffset();

    this.translate(this.maxOffset);
  }, 200);
}

export default function () {
  const elements = document.querySelectorAll(ELEMENT_CLASS);

  if (elements.length > 0) {
    [...elements].forEach(element => new Drawer(element));
  }
}
