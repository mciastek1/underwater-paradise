import ImageLoader from '../utils/image-loader';

const ELEMENT_CLASS = '.js-splash-screen';
const IMAGE_CLASS = '.js-image-loader';
const HIDDEN_CLASS_NAME = 'is-hidden';

function setSplashScreen(element) {
  const images = document.querySelectorAll(IMAGE_CLASS);
  const imageLoader = new ImageLoader(images);

  imageLoader.observe()
    .then(() => element.classList.add(HIDDEN_CLASS_NAME));
}

export default function () {
  const element = document.querySelector(ELEMENT_CLASS);

  if (element) {
    setSplashScreen(element);
  }
}
