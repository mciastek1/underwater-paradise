import prefixes from '../utils/prefix-detector';

export const ELEMENT_CLASS = '.js-shuffler';
const ITEM_CLASS = '.js-shuffler-item';

export default class Shuffler {
  constructor(element) {
    this.element = element;
    this.items = this.element.querySelectorAll(ITEM_CLASS);

    this.currentItem = 0;
  }

  translate(posY) {
    this.element.style[prefixes.transform] = `translate3d(0, ${posY}px, 0)`;
  }

  updatePosition(newIndex) {
    const itemHeight = this.items[0].getBoundingClientRect().height;

    this.currentItem = newIndex;
    this.translate(itemHeight * this.currentItem * -1);
  }
}
