const ELEMENT_CLASS = '.js-fav-button';
const ACTIVE_CLASS_NAME = 'is-active';

class FavButton {
  constructor(element) {
    this.element = element;

    this.init();
  }

  init() {
    this.attachEvents();
  }

  attachEvents() {
    this.element.addEventListener('touchend', this.handleTouchEnd);
  }

  handleTouchEnd = () => {
    this.element.classList.toggle(ACTIVE_CLASS_NAME);
  };
}

export default function () {
  const elements = document.querySelectorAll(ELEMENT_CLASS);

  if (elements.length > 0) {
    [...elements].forEach(element => new FavButton(element));
  }
}
