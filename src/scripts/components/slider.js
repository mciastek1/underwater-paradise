import Shuffler, { ELEMENT_CLASS as SHUFFLER_ELEMENT_CLASS } from './shuffler';

import Carousel from '../utils/carousel';
import store from '../utils/store';

const ELEMENT_CLASS = '.js-slider';
const BUTTON_CLASS = '.js-slider-bullet';
const ACTIVE_CLASS_NAME = 'is-active';

const settings = {
  containerSelector: '.js-slider-container',
  itemSelector: '.js-slider-item',
};

class Slider extends Carousel {
  constructor(element, options) {
    super(element, options);

    this.bullets = this.element.querySelectorAll(BUTTON_CLASS);
    this.authorsList = this.element.querySelector(SHUFFLER_ELEMENT_CLASS);

    this.store = store;
    this.shuffler = new Shuffler(this.authorsList);
  }

  init() {
    super.init();

    this.bullets[this.currentSlide].classList.add(ACTIVE_CLASS_NAME);
  }

  slide() {
    const nextIndex = this.currentSlide + this.direction;
    const nextSlide = Math.min(Math.max(nextIndex, 0), this.items.length - 1);
    const nextOffset = this.items[nextSlide].offsetLeft * -1;

    this.bullets[this.currentSlide].classList.remove(ACTIVE_CLASS_NAME);

    this.currentSlide = nextSlide;
    this.currentSlideOffset = nextOffset;

    this.bullets[this.currentSlide].classList.add(ACTIVE_CLASS_NAME);

    this.animate();
    this.translate(this.currentSlideOffset);
    this.shuffler.updatePosition(this.currentSlide);
  }
}

export default function () {
  const elements = document.querySelectorAll(ELEMENT_CLASS);

  if (elements.length > 0) {
    [...elements].forEach(element => new Slider(element, settings).init());
  }
}
