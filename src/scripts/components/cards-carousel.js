import Carousel from '../utils/carousel';
import store from '../utils/store';
import prefixes from '../utils/prefix-detector';

const ELEMENT_SELECTOR = '.js-cards';

const settings = {
  containerSelector: '.js-cards-container',
  itemSelector: '.js-cards-item',
  touchesNumber: 2,
};

class CardsCarousel extends Carousel {
  constructor(element, options) {
    super(element, options);

    this.store = store;
  }

  scale(scaleRatio) {
    this.items[this.currentSlide].style[prefixes.transform] = `scale(${scaleRatio}, ${scaleRatio})`;
  }

  slide() {
    super.slide();
    this.scale(1);
  }

  handleTouchMove({ x, y }) {
    super.handleTouchMove({ x, y });

    const containerWidth = this.container.getBoundingClientRect().width;
    const newScale = (containerWidth - Math.abs(x)) / containerWidth;
    const scale = Math.min(Math.max(newScale, 0.7), 1);

    if (!this.isScrolling && !this.store.drawerOpen) {
      this.scale(scale);
    }
  }

  handleTouchEnd({ time }, { x }) {
    const duration = Date.now() - time;
    const containerWidth = this.container.getBoundingClientRect().width;

    const shouldSlide = (duration < 300 && Math.abs(x) > 25) || (Math.abs(x) > containerWidth / 3);

    if (!this.isScrolling && !this.store.drawerOpen) {
      if (shouldSlide) {
        this.slide();
      } else {
        this.animate();
        this.translate(this.currentSlideOffset);
        this.scale(1);
      }
    }
  }

  handleResize() {
    super.handleResize();
    this.scale(1);
  }
}

export default function () {
  const elements = document.querySelectorAll(ELEMENT_SELECTOR);

  if (elements.length > 0) {
    [...elements].forEach(element => new CardsCarousel(element, settings).init());
  }
}
