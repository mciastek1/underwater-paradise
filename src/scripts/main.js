import initSliders from './components/slider';
import initDrawers from './components/drawer';
import initCardCarousel from './components/cards-carousel';
import initFavButton from './components/fav-button';
import initSplashScreen from './components/splash-screen';

document.addEventListener('DOMContentLoaded', () => {
  initSplashScreen();
  initCardCarousel();
  initSliders();
  initDrawers();
  initFavButton();
});
