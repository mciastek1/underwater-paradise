const transforms = ['transform', 'msTransform', 'webkitTransform', 'mozTransform', 'oTransform'];
const transitions = ['webkitTransition', 'transition'];

function getSupportedPropertyName(properties) {
  for (let i = 0; i < properties.length; i + 1) {
    if (typeof document.body.style[properties[i]] !== 'undefined') {
      return properties[i];
    }
  }
  return null;
}

function getTransitionEndName() {
  const prefixedTransition = getSupportedPropertyName(transitions);

  return (prefixedTransition === 'webkitTransition') ? 'webkitTransitionEnd' : 'transitionend';
}

export default {
  transform: getSupportedPropertyName(transforms),
  transitionEnd: getTransitionEndName(),
};
