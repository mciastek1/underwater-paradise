function waitForLoad(image) {
  return new Promise((resolve, reject) => {
    if (!image.complete) {
      // eslint-disable-next-line no-param-reassign
      image.onload = () => resolve(image);

      // eslint-disable-next-line no-param-reassign
      image.onerror = () => reject(image);
    } else {
      resolve(image);
    }
  });
}

export default class ImageLoader {
  constructor(images) {
    this.images = images;
  }

  observe() {
    return Promise.all([...this.images].map(waitForLoad));
  }
}
