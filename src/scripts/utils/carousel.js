import debounce from 'lodash/debounce';

import Swiper from './swiper';
import prefixes from './prefix-detector';
import resizeEventName from './resize-event-name';

const CONTAINER_CLASS = '.js-cards-container';
const ITEM_CLASS = '.js-cards-item';
const ANIMATION_CLASS_NAME = 'is-animate';

const defaults = {
  containerSelector: CONTAINER_CLASS,
  itemSelector: ITEM_CLASS,
  animationClassName: ANIMATION_CLASS_NAME,
  touchesNumber: 1,
};

export default class Carousel {
  constructor(element, options) {
    this.element = element;

    this.settings = {
      ...defaults,
      ...options,
    };

    this.container = this.element.querySelector(this.settings.containerSelector);
    this.items = this.element.querySelectorAll(this.settings.itemSelector);

    this.swiper = new Swiper(this.element, {
      touchesNumber: this.settings.touchesNumber,
    });

    this.direction = 1;
    this.currentSlide = 0;
    this.currentSlideOffset = 0;

    this.isScrolling = false;
  }

  init() {
    this.attachEvents();
  }

  attachEvents() {
    this.swiper.addHandler('touchstart', touchOffset => this.handleTouchStart(touchOffset));
    this.swiper.addHandler('touchmove', touchDelta => this.handleTouchMove(touchDelta));
    this.swiper.addHandler('touchend', (touchOffset, touchDelta) => this.handleTouchEnd(touchOffset, touchDelta));
    window.addEventListener(resizeEventName, () => debounce(this.handleResize.bind(this), 200)());
  }

  translate(posX) {
    this.container.style[prefixes.transform] = `translate3d(${posX}px, 0, 0)`;
  }

  slide() {
    const nextIndex = this.currentSlide + this.direction;
    const nextSlide = Math.min(Math.max(nextIndex, 0), this.items.length - 1);
    const nextOffset = this.items[nextSlide].offsetLeft * -1;

    this.currentSlide = nextSlide;
    this.currentSlideOffset = nextOffset;

    this.animate();
    this.translate(this.currentSlideOffset);
  }

  animate() {
    this.container.addEventListener(prefixes.transitionEnd, this.handleTransitionEnd);
    this.container.classList.add(this.settings.animationClassName);
  }

  handleTouchStart() {
    this.isScrolling = false;
  }

  handleTouchMove({ x, y }) {
    this.direction = (x > 0) ? -1 : 1;
    this.isScrolling = Math.abs(x) < Math.abs(y);

    if (!this.isScrolling && !this.store.drawerOpen) {
      this.translate(x + this.currentSlideOffset);
    }
  }

  handleTouchEnd({ time }, { x }) {
    const duration = Date.now() - time;
    const containerWidth = this.container.getBoundingClientRect().width;

    const shouldSlide = (duration < 300 && Math.abs(x) > 25) || (Math.abs(x) > containerWidth / 3);

    if (!this.isScrolling && !this.store.drawerOpen) {
      if (shouldSlide) {
        this.slide();
      } else {
        this.animate();
        this.translate(this.currentSlideOffset);
      }
    }
  }

  handleResize() {
    this.translate(this.items[this.currentSlide].offsetLeft * -1);
  }

  handleTransitionEnd = () => {
    this.container.classList.remove(this.settings.animationClassName);
    this.container.removeEventListener(prefixes.transitionEnd, this.handleTransitionEnd);
  };
}
