import isFunction from 'lodash/isFunction';

const defaults = {
  touchesNumber: 1,
};

function collectCoords(coords, { pageX, pageY }) {
  return {
    x: coords.x + pageX,
    y: coords.y + pageY,
  };
}

function getTouchCoords(touches) {
  const startCoords = {
    x: 0,
    y: 0,
  };

  const { x, y } = [...touches].reduce(collectCoords, startCoords);

  return {
    x: x / touches.length,
    y: y / touches.length,
  };
}

export default class Swiper {
  constructor(element, options) {
    this.element = element;

    this.settings = {
      ...defaults,
      ...options,
    };

    this.handlers = {};

    this.init();
  }

  init() {
    this.element.addEventListener('touchstart', this.handleTouchStart);
    this.element.addEventListener('touchmove', this.handleTouchMove);
    this.element.addEventListener('touchend', this.handleTouchEnd);
  }

  addHandler(eventType, callback) {
    this.handlers[eventType] = callback;
  }

  handleTouchStart = (event) => {
    const { touches } = event;

    if (touches.length !== this.settings.touchesNumber) return;

    const { x, y } = getTouchCoords(touches);

    this.touchOffset = {
      x,
      y,
      time: Date.now(),
    };

    this.touchDelta = {};

    if (isFunction(this.handlers.touchstart)) {
      this.handlers.touchstart(this.touchOffset, event);
    }

    event.preventDefault();
  };

  handleTouchMove = (event) => {
    const { touches } = event;

    if (touches.length !== this.settings.touchesNumber) return;

    const { x, y } = getTouchCoords(touches);

    this.touchDelta = {
      x: x - this.touchOffset.x,
      y: y - this.touchOffset.y,
    };

    if (isFunction(this.handlers.touchmove)) {
      this.handlers.touchmove(this.touchDelta, event);
    }

    event.preventDefault();
  };

  handleTouchEnd = (event) => {
    const { changedTouches } = event;

    if (changedTouches.length !== this.settings.touchesNumber) return;

    if (isFunction(this.handlers.touchend)) {
      this.handlers.touchend(this.touchOffset, this.touchDelta, event);
    }
  };
}
