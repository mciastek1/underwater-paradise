const addSuffix = (name, suffix) => {
  const extension = name.substr(name.lastIndexOf('.'), name.length - 1);
  const extensionLess = name.substr(0, name.lastIndexOf('.'));
  return extensionLess + suffix + extension;
};

module.exports.register = (Handlebars) => {
  Handlebars.registerHelper('suffix-filename', (name, suffix) => addSuffix(name, suffix));
};
